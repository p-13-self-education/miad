class Hangman {
  constructor(word, tries, onSuccess, onFail, onRetry) {
    this.HIDDEN_LETTER = '*';
    this.letters = word.split('');
    this.tries = tries;
    this.onSuccess = onSuccess;
    this.onFail = onFail;
    this.onRetry = onRetry;
    this.mask = new Array(this.letters.length).fill(this.HIDDEN_LETTER);
  }

  tryToOpen(l) {
    return this.checkLetter(l)
      ? this.checkSuccess(l)
      : this.checkFailed();
  }

  checkLetter(l) {
    return this.letters.includes(l) && !this.mask.includes(l);
  }

  checkSuccess(l) {
    this.updateMask(l);
    return this.mask.includes(this.HIDDEN_LETTER)
      ? this.getMask()
      : this.onSuccess();
  }

  checkFailed() {
    this.registerWrongAnswer();
    return this.tries
      ? this.onRetry(this.tries)
      : this.onFail();
  }

  getMask() {
    return toStr(this.mask);
  }

  updateMask(l) {
    this.letters.forEach((letter, i) => {
      if (letter === l) {
        this.mask[i] = l;
      }
    });
    return toStr(this.mask);
  }

  registerWrongAnswer() {
    this.tries -= 1;
  }

  end() {
    return toStr(this.letters);
  }
}

function toStr(arr) {
  return arr.join('');
}

module.exports = Hangman;
