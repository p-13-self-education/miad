require('dotenv').config();
const TelegramBot = require('node-telegram-bot-api');
const ngrok = require('ngrok');
const WordsService = require('./words.service');
const Hangman = require('./hangman');

const {
  TELEGRAM_TOKEN,
  RANDOM_WORD_API_URL,
  WORDS_API_URL,
  PORT,
} = process.env;
const bot = new TelegramBot(TELEGRAM_TOKEN, { polling: true });
const wordsService = new WordsService(RANDOM_WORD_API_URL, WORDS_API_URL);
const games = new Map();
const TRIES = 3;

const onSuccess = (id) => () => `Congratulations! The word was ${gameOver(id)}`;
const onFail = (id) => () => `You lose! The word was ${gameOver(id)}`;
const onRetry = (tries) => `Oops! You have ${tries === 1 ? 'the last' : 'another'} try.`;

ngrok
  .connect(PORT)
  .then((u) => {
    console.log(`Game tunneled at ${u}`);
  });

bot.onText(/^\/start$/, ({ chat: { id } }) => {
  wordsService
    .getRandomWord()
    .then((word) => {
      const hangman = new Hangman(word, TRIES, onSuccess(id), onFail(id), onRetry);
      games.set(id, hangman);
      bot.sendMessage(id, hangman.end());
    });
});

bot.onText(/^\w$/, ({ chat: { id } }, [letter]) => {
  if (games.has(id)) {
    const hangman = games.get(id);
    bot.sendMessage(id, hangman.tryToOpen(letter));
  }
});

bot.onText(/^\/end$/, ({ chat: { id } }) => {
  if (games.has(id)) {
    bot.sendMessage(id, gameOver(id));
  }
});

function gameOver(id) {
  const answer = games.get(id).end();
  games.delete(id);
  return answer;
}
