const request = require('./request');

class WordsService {
  constructor(randomWordApiUrl) {
    this.routes = {
      randomWord: `${randomWordApiUrl}/word`,
    };
  }

  getRandomWord() {
    return request(this.routes.randomWord)
      .then(JSON.parse)
      .then(([word]) => word);
  }
}

module.exports = WordsService;
