# Hangman telegram bot

Project setup

* create .env file in project root directory with the next lines:

```
TELEGRAM_TOKEN=<YOUR_TELEGRAM_TOKEN>
RANDOM_WORD_API_URL='https://random-word-api.herokuapp.com'
PORT=5000
```

* run:

```
npm ci && npm run start
```
